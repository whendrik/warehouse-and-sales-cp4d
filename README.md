# Warehouse and Sales examples on CP4D

Notebooks showing toy examples for warehouse and sales related use cases in CP4D.

1. Basic `Db2` connectivity (pull and push)
2. Load Sales data from `Db2`, and use **XGBoost** to predict on Time Series data.
3. Load Client data from `Db2`, and use **CPLEX** to optimize the right offer to right clients.
4. Load Client data from `Db2`, and use customer segmentation techniques to find **clusters**.

---

